import React from "react";
import { Button, StyleSheet, Text, View } from "react-native";

// :: main: App Component
export default function App() {
  //:: onButtonHandler
  function onButtonHandler(params) {
    console.log("onbuttonHandler");
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text}>welcome to nikhil-menan zone</Text>
      <Button title="iamnikhilmenan" onPress={onButtonHandler} />
    </View>
  );
}

// :: main: CSS StyleSheet
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  text: {
    backgroundColor: "blue",
    color: "white",
    borderRadius: 100,
    padding: 12,
    margin: 12,
  },
});
